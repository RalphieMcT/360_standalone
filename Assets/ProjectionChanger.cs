﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Cameras;


/// <summary>
/// Changes the MeshFilter of the videowall so that the viewer can see either flat or 360 videos.
/// </summary>
public class ProjectionChanger : MonoBehaviour {
    private const int cylinderYScaling = 15; //height of cylinder should align with vertical view frustrum
    GameObject videoWall;
    Mesh cylinderMesh;
    Mesh sphereMesh;
    MeshRenderer cylinderMask;
    bool isSphereProjection;

	// Use this for initialization
	void Start ()
    {
        isSphereProjection = true;
        videoWall = GameObject.Find("VideoWall");
        GameObject cylinderObject = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        GameObject sphereObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        cylinderMesh = cylinderObject.GetComponent<MeshFilter>().mesh;
        sphereMesh = sphereObject.GetComponent<MeshFilter>().mesh;
        cylinderObject.SetActive(false);
        sphereObject.SetActive(false);
        cylinderMask = GameObject.Find("VideoWall/CylinderMask").GetComponent<MeshRenderer>();
        GetComponent<Button>().onClick.AddListener(() => { ChangeProjection(); });
    }

    private void ChangeProjection()
    {
        if (isSphereProjection)
            toCylinderProjection();
        else
            toSphereProjection();
    }

    private void toSphereProjection()
    {
        MeshFilter meshFilter = videoWall.GetComponent<MeshFilter>();
        meshFilter.mesh = sphereMesh;
        videoWall.transform.localScale = Vector3.one * 50;
        isSphereProjection = true;
    }

    private void toCylinderProjection()
    {
        isSphereProjection = false;
        MeshFilter meshFilter = videoWall.GetComponent<MeshFilter>();
        meshFilter.mesh = cylinderMesh;
        videoWall.transform.localScale = new Vector3(50, cylinderYScaling, 50);
        GameObject rig = GameObject.Find("FreeLookCameraRig");
        rig.GetComponent<FreeLookCam>().ResetTransform();
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown("ChangeProjection"))
            ChangeProjection();

        cylinderMask.enabled = !isSphereProjection;
	}
}
