﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine;

public class ButtonClickHandler : MonoBehaviour {

	public void OnClick()
    {
        VideoScript videoScript;
        GameObject videoWall = GameObject.Find("VideoWall");
        videoScript = videoWall.GetComponent<VideoScript>();
        videoScript.StartVideo("file://" + Application.streamingAssetsPath + "/TestVideos/" + transform.name);

        GameObject.Find("Expand").GetComponent<PanelExpander>().ToggleVisible();
    }
}
