﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIManager : MonoBehaviour {
    Animator controlPanel;
    Animator optionsPanel;
    [SerializeField] PanelExpander pe;

	// Use this for initialization
	void Start () {
        controlPanel = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetMouseButtonUp(1))
            CloseButtonListAndOptionPanel();
    }

    public void CloseButtonListAndOptionPanel() {
        ToggleVisible();
        if(pe.optionsPanel.GetBool("isExpanded")) {
            pe.ToggleVisible();
        }
    }

    public void ToggleVisible() {
        bool currentState = controlPanel.GetBool("isVisible");
        controlPanel.SetBool("isVisible", !currentState);
    }
}
