﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Hidden/Warping"
{	
	Properties
	{
		_MainTex("Texture", 2D) = "black" {}
		_WarpTex("Warp", 2D) = "black" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			sampler2D _MainTex;
			sampler2D _WarpTex;
			float4 _MainTex_TexelSize;

			float4 frag(v2f i) : SV_Target
			{
				//Flip texture if DirectX
			
				#if UNITY_UV_STARTS_AT_TOP
					i.uv.y = 1 - i.uv.y;
				#endif
			
				//compensate for 90degree texture rotation
				float4 tmpX = i.uv.x;
				float4 tmpY = i.uv.y;
				i.uv.x = 1 - i.uv.y;
				i.uv.y = tmpX;
				
				//i.uv.y = 1 - i.uv.y;
				float4 WarpCoordinate = tex2D(_WarpTex, i.uv);
				//un-compensate for texture rotation
				WarpCoordinate.y = 1 - WarpCoordinate.y;

				//flip coordinates to correct orientation
				float4 WarpedTexture;
				//if warp texture is black, render black
				if (all(WarpCoordinate) == 0){
					WarpedTexture = float4(0.0,0.0,0.0,0.0);
				}
				else{ 
					WarpedTexture = tex2D(_MainTex, WarpCoordinate);
				}
				return WarpedTexture;
				//return WarpCoordinate;
			}
			ENDCG
		}
	}
Fallback off
}
