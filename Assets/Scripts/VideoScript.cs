﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using System;
using SFB;

public class VideoScript : MonoBehaviour
{
    VideoPlayer vp;
    public string VideoName { get; set; }
    
	void Start ()
    {
        vp = GetComponent<VideoPlayer>();
        vp.source = VideoSource.Url;
        vp.audioOutputMode = VideoAudioOutputMode.AudioSource;
        vp.SetTargetAudioSource(0, GetComponent<AudioSource>());
        
        GameObject playButton = GameObject.Find("Canvas/VideoControls/Play");
        GameObject stopButton = GameObject.Find("Canvas/VideoControls/Stop");
        GameObject pauseButton = GameObject.Find("Canvas/VideoControls/Pause");
        Slider slider = GameObject.Find("Canvas/VideoControls/Slider").GetComponent<Slider>();
        playButton.GetComponent<Button>().onClick.AddListener(() => { vp.Play(); });
        stopButton.GetComponent<Button>().onClick.AddListener(() => { vp.Stop(); });
        pauseButton.GetComponent<Button>().onClick.AddListener(() => { vp.Pause(); });
    }

    internal void StartVideo(string path)
    {
        vp.url = "file://" + path;
        vp.Play();
    }

    /// <summary>
    /// Skips a numFrames amount of videoframes
    /// </summary>
    /// <param name="numFrames">How many frames to skip. Negative values seeks backwards</param>
    public void Seek(int numFrames)
    {
        vp.frame += numFrames;
    }

    /// <summary>
    /// Set video time
    /// </summary>
    /// <param name="progress"></param>
    public void SetVideoTime(float x)
    {
        Slider slider = GameObject.Find("Canvas/VideoControls/Slider").GetComponent<Slider>();
        vp.time = vp.clip.length * x;
    }

    void Update()
    {
        if (Input.GetButtonDown("PlayToggle"))
        {
            if (vp.isPlaying)
                vp.Pause();
            else
                vp.Play();
        }

        if ( (Input.GetButton("rctrl") || Input.GetButton("lctrl")) && Input.GetButton("Vertical"))
        {
            Debug.Log("up/down");
        }

        //ctrl+left/right seek forward/backwards in the movie
        if ( (Input.GetButton("rctrl") || Input.GetButton("lctrl")) && Input.GetButton("Horizontal"))
        {
            if (Input.GetAxisRaw("Horizontal") > 0)
                Seek(5 * (int)vp.frameRate);
            else
                Seek(-5 * (int)vp.frameRate);
        }

        //ctrl+o open file dialogue
        if (Input.GetButtonDown("Open"))
        {
            string[] filePath = StandaloneFileBrowser.OpenFilePanel("Open File", "", "", true);
            if (filePath.Length > 0)
            {
                string path = filePath[0];
                GameObject progressPanel = GameObject.Find("Canvas/ProgressPanel");
                GameObject guiManager = GameObject.Find("Canvas/VideoControls");
                try
                {
                    GameObject videoWall = GameObject.Find("VideoWall");
                    VideoScript vs = videoWall.GetComponent<VideoScript>();
                    vs.StartVideo(path);
                    guiManager.GetComponent<GUIManager>().CloseButtonListAndOptionPanel();
                }
                catch (System.Exception e)
                {
                    Debug.LogException(e);
                    Debug.Log("Input Path: " + path);
                    Debug.LogError("Cant import that file: " + e.ToString());
                }
            }
        }
    }

    private void OnGUI()
    {
        if (!vp.isPlaying)
        {
            int x = 0;
            int y = Display.displays[0].renderingHeight/2;
            int width = 400;
            for (int i = 0; i < Display.displays.Length; i++)
            {
                x += Display.displays[i].renderingWidth;
            }
            x = x / 2;
            GUI.TextArea(new Rect(x-width/2, y, width, 120), "Keybinds\n"+ 
                "Turn\t\tArrows\n"+ 
                "Open File\t\tO\n" + 
                "Projection\t\tP" + 
                "\nPlay/Pause\tSpace" + 
                "\nSkip 5 sec\t\tCtrl+Left/Right Arrows" + 
                "\nReset Position:\t"+"Backspace");
        }
    }
}
