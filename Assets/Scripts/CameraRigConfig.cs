﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;

/// <summary>
/// This class holds the data describing how the camera rig should be instantiated
/// </summary>
public class CameraRigConfig
{
    public int UILocation;
    public string[] cameraConfigPaths;
    public int[] usedProjectors;
    public string cameraFilesPath;

    public CameraRigConfig(CameraSpawner.DisplayType type)
    {
        cameraFilesPath = Path.Combine(Application.dataPath, "StreamingAssets/CameraFiles");
        if (type == CameraSpawner.DisplayType.SingleMonitor)
        {
            cameraConfigPaths = Directory.GetDirectories(cameraFilesPath).Skip(5).Take(1).ToArray();
            UILocation = 0;
            usedProjectors = new int[1] { 0 };
        }
        else if (type == CameraSpawner.DisplayType.DoubleMonitor)
        {
            cameraConfigPaths = Directory.GetDirectories(cameraFilesPath).Skip(5).Take(2).ToArray();
            cameraConfigPaths[1] = Directory.GetDirectories(cameraFilesPath).Skip(6).Take(1).ToArray()[0];
            UILocation = 0;
            usedProjectors = new int[2] { 0, 1 };
        }
        else if (type == CameraSpawner.DisplayType.MultiProjector8)
        {
            cameraConfigPaths = Directory.GetDirectories(cameraFilesPath).Skip(2).Take(8).ToArray();
            UILocation = 3;
            usedProjectors = new int[8] { 0, 1, 2, 3, 4, 5, 6, 7};
        }
        else if (type == CameraSpawner.DisplayType.MultiProjector12)
        {
            cameraConfigPaths = Directory.GetDirectories(cameraFilesPath).Skip(0).Take(12).ToArray();
            UILocation = 3;
            usedProjectors = new int[12] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
        }
    }
}

