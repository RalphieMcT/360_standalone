﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class FfmpegWrapper
{
    private List<String> m_args = new List<String>();
    private string outputPath;
    
    public FfmpegWrapper setInputFile(string path)
    {
        m_args.Insert(0, " -i " + "\"" + path + "\"");
        return this;
    }

    public FfmpegWrapper setOutputFilePath(string path)
    {
        outputPath = " " + "\"" + path + "\"";
        return this;
    }

    public FfmpegWrapper setAudioCodec(string name)
    {
        m_args.Add(" -acodec " + name);
        return this;
    }

    public FfmpegWrapper setVideoCodec(string name)
    {
        m_args.Add(" -vcodec " + name);
        return this;
    }

    public FfmpegWrapper forceFileFormat(string format)
    {
        m_args.Add(" -f " + format);
        return this;
    }

    /// <summary>
    /// set overwrite to true to overwrite videos without asking, 
    /// false to skip importing if file exists
    /// </summary>
    /// <param name="overwrite"></param>
    /// <returns></returns>
    public FfmpegWrapper setForceOverwrite(bool overwrite)
    {
        if (overwrite)
            m_args.Add(" -y ");
        else
            m_args.Add(" -n ");
        return this;
    }

    /// <summary>
    /// sets the height and width of the video in pixels
    /// </summary>
    /// <param name="size">pixel height and width of video</param>
    /// <returns></returns>
    public FfmpegWrapper setVideoSize(int size)
    {
        m_args.Add(" -s " + size.ToString() + "x" + size.ToString());
        return this;
    }

    /// <summary>
    /// sets the height and width of the video in pixels
    /// </summary>
    /// <returns></returns>
    public FfmpegWrapper setVideoSize(int height, int width)
    {
        m_args.Add(" -s " + width.ToString() + "x" + height.ToString());
        return this;
    }
    /// <summary>
    /// set a video quality 0-10 where 10 is best quality
    /// </summary>
    /// <param name="quality"></param>
    /// <returns></returns>
    public FfmpegWrapper setVideoQuality(int quality)
    {
        m_args.Add(" -qscale:v " + quality.ToString());
        return this;
    }

    override public string ToString()
    {
        string args = "";
        foreach (string arg in m_args)
        {
            args += arg;
        }
        args += outputPath;
        return args;
    }

    /// <summary>
    /// Adds the -vn option in order to ignore video input
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public FfmpegWrapper AudioOnly()
    {
        m_args.Add(" -vn ");
        return this;
    }
}
