﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

[ExecuteInEditMode]
public class Blending : ImageEffectBase{
    
    protected override void Start()
    {
        base.Start();

        string startOfPath = Path.Combine(Application.dataPath, @"StreamingAssets/CameraFiles");
        startOfPath = Path.Combine(startOfPath, gameObject.name);
        string endOfPath = gameObject.GetComponent<CameraConfig>().BlendTexture;
        endOfPath = endOfPath.Substring(2, endOfPath.Length - 2);
        string fullTexturePath = Path.Combine(startOfPath, endOfPath);

        Texture2D blendTexture = TextureLoader.LoadRawTexture(fullTexturePath, TextureFormat.RFloat);
        material.SetTexture("_BlendTex", blendTexture);
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, material);
    }
}
