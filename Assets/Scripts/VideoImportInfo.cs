﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class VideoImportInfo
{
    struct TimeData
    {
        public double now, end;
    }
    TimeSpan time;
    TimeSpan duration;
    public string inputPath;
    public string outputPath;
    public string fileName;
    public bool isImporting;

    public double Progress {
        get {
            return CalculatePercentageDone();
        }
    }

    private double CalculatePercentageDone()
    {
        double percentage = time.TotalSeconds / duration.TotalSeconds;
        return percentage;
    }

    public VideoImportInfo ParseLine(string line)
    {
        if (line.TrimStart().StartsWith("frame="))
        {
            time = TimeSpan.Parse(GetValueBetweenKeys(line, "time=", "bitrate="));
            //Debug.Log(time.ToString());
            isImporting = true;
        }
        else if (line.TrimStart().StartsWith("Duration"))
        {
            //Debug.Log(line);
            string[] KVPairs = line.Split(','); //array of Key/value pairs
            for (int i = 0; i < KVPairs.Length; i++)
            {
                string[] SplitKVPair = KVPairs[i].Split(':');
                string key = SplitKVPair[0];
                string value = "";

                if (SplitKVPair.Length > 2)
                {
                    //sometimes length is >2 because of "Duration:00:01:39.87"
                    value = string.Join(":", SplitKVPair, 1, SplitKVPair.Length - 1);
                    duration = TimeSpan.Parse(value);
                }
            }
        }
        Debug.Log(line);
        return this;
    }

    private string GetValueBetweenKeys(string line, string key1, string key2)
    {
        int startOfKey = line.IndexOf(key1);
        int startOfValue = startOfKey + key1.Length;
        int endOfValue = line.IndexOf(key2);
        return line.Substring(startOfValue, endOfValue - startOfValue);
    }
}
