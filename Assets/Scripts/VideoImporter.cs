﻿/*
This class imports an mp4 movie to Assets/Resources/ either as a series of .jpegs or .ogg. depends on ffmpeg via windows command line
Based on http://answers.unity3d.com/questions/1127023/running-command-line-action-through-c-script.html
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System;

public class VideoImporter
{
    public VideoImportInfo videoImportInfo = new VideoImportInfo();
    private string ffmpegPath;
    private string texconvPath;

    /// <summary>
    /// imports the video as a series of DDS texture files 
    /// </summary>
    /// <param name="inputVideoPath">The location of the video to convert</param>
    /// <param name="videoStorageType">The type of DDS texture to store the video as</param>
    public VideoImporter(string inputVideoPath, TextureFormat videoStorageType)
    {
        ffmpegPath = Path.Combine(Application.streamingAssetsPath, @"ffmpeg\bin\ffmpeg.exe");
        texconvPath = Path.Combine(Application.streamingAssetsPath, @"texconv\texconv.exe");
        videoImportInfo.inputPath = inputVideoPath;
        videoImportInfo.fileName = Path.GetFileName(Path.GetFileNameWithoutExtension(videoImportInfo.inputPath));
        videoImportInfo.outputPath = Path.Combine(Application.streamingAssetsPath, "DDSVideos" + "/" + videoImportInfo.fileName);
        Directory.CreateDirectory(videoImportInfo.outputPath);
        JPEGImport();
    }

    /// <summary>
    /// imports the video to a .ogg file
    /// </summary>
    /// <param name="inputVideoPath">The location of the video to convert</param>
    public VideoImporter(string inputVideoPath)
    {
        ffmpegPath = Path.Combine(Application.streamingAssetsPath, @"ffmpeg\bin\ffmpeg.exe");
        videoImportInfo.inputPath = inputVideoPath; // surrounds the path with "
        videoImportInfo.fileName = Path.GetFileName(Path.GetFileNameWithoutExtension(videoImportInfo.inputPath));
        videoImportInfo.outputPath = Path.Combine(Application.streamingAssetsPath, "OggVideos" + "/" + videoImportInfo.fileName + ".ogg");
        OggImport();
    }

    private void OggImport()
    {
        FfmpegWrapper FfmpegArguments = new FfmpegWrapper();
        FfmpegArguments.setInputFile(videoImportInfo.inputPath);
        FfmpegArguments.setAudioCodec("libvorbis");
        FfmpegArguments.setVideoCodec("libtheora");
        FfmpegArguments.forceFileFormat("ogg");
        FfmpegArguments.setVideoSize(1024);
        FfmpegArguments.setVideoQuality(7);
        FfmpegArguments.setForceOverwrite(true);
        FfmpegArguments.setOutputFilePath(videoImportInfo.outputPath);

        Process process = new Process();
        process.EnableRaisingEvents = true;
        process.StartInfo.CreateNoWindow = true;
        process.StartInfo.UseShellExecute = false;
        process.StartInfo.RedirectStandardError = true;
        process.StartInfo.WorkingDirectory = Path.GetDirectoryName(ffmpegPath);
        process.StartInfo.FileName = ffmpegPath;
        process.StartInfo.Arguments = FfmpegArguments.ToString();
        process.Exited += new EventHandler(OnAudioImportComplete);

        StartProcessInNewThread(process);
    }

    private void JPEGImport()
    {
        FfmpegWrapper ffmpegArguments = new FfmpegWrapper();
        ffmpegArguments.setInputFile(videoImportInfo.inputPath);
        ffmpegArguments.setVideoSize(1024, 2048);
        ffmpegArguments.setVideoQuality(10);
        ffmpegArguments.setOutputFilePath(videoImportInfo.outputPath + "/frame%4d.jpg");

        Process process = new Process();
        process.EnableRaisingEvents = true;
        process.StartInfo.CreateNoWindow = true;
        process.StartInfo.UseShellExecute = false;
        process.StartInfo.RedirectStandardError = true;
        process.StartInfo.WorkingDirectory = Path.GetDirectoryName(ffmpegPath);
        process.StartInfo.FileName = ffmpegPath;
        process.StartInfo.Arguments = ffmpegArguments.ToString();
        process.Exited += new EventHandler(OnJPEGImportComplete);

        StartProcessInNewThread(process);
    }

    private void AudioImport()
    {
        FfmpegWrapper ffmpegArguments = new FfmpegWrapper();
        ffmpegArguments.setInputFile(videoImportInfo.inputPath);
        ffmpegArguments.AudioOnly();
        ffmpegArguments.setForceOverwrite(true);
        ffmpegArguments.setOutputFilePath(videoImportInfo.outputPath + @"/audio.wav");

        Process process = new Process();
        process.EnableRaisingEvents = true;
        process.StartInfo.CreateNoWindow = true;
        process.StartInfo.UseShellExecute = false;
        process.StartInfo.RedirectStandardError = false;
        process.StartInfo.WorkingDirectory = Path.GetDirectoryName(ffmpegPath);
        process.StartInfo.FileName = ffmpegPath;
        process.StartInfo.Arguments = ffmpegArguments.ToString();
        process.Exited += new EventHandler(OnAudioImportComplete);

        StartProcessInNewThread(process);
    }
    
    private void ConvertJPEGtoDDS()
    {
        TexconvWrapper texconvArguments = new TexconvWrapper();
        texconvArguments.SetInputPath(videoImportInfo.outputPath, "*.jpg");
        texconvArguments.SetOutputPath(videoImportInfo.outputPath);
        texconvArguments.CPUOnly();
        texconvArguments.ForceOverwrite();
        texconvArguments.SetFormat("BC1_UNORM");
        texconvArguments.SetNumberOfMipMaps(1);
        texconvArguments.EnableTiming();
        texconvArguments.SetTexconvPath(texconvPath);
        texconvArguments.VerticalFlip(); //unity reads dds upside down
        UnityEngine.Debug.Log(texconvArguments.ToString());

        Process process = new Process();
        process.EnableRaisingEvents = true;
        process.StartInfo.CreateNoWindow = true;
        process.StartInfo.UseShellExecute = false;
        process.StartInfo.RedirectStandardError = true;
        process.StartInfo.WorkingDirectory = Path.GetDirectoryName(texconvPath);
        process.StartInfo.FileName = texconvPath;
        process.StartInfo.Arguments = texconvArguments.ToString();
        process.Exited += new EventHandler(OnImportComplete);
        process.Exited += new EventHandler(DeleteFilesInDirectory);

        StartProcessInNewThread(process);
    }

    private void OnJPEGImportComplete(object sender, EventArgs e)
    {
        UnityEngine.Debug.Log("JPEG Import Complete");
        AudioImport();
        ConvertJPEGtoDDS();
    }

    private void OnImportComplete(object sender, EventArgs e)
    {
        videoImportInfo.isImporting = false;
        UnityEngine.Debug.Log("Import Complete");
    }

    private void DeleteFilesInDirectory(object sender, EventArgs e)
    {
        string[] files = Directory.GetFiles(videoImportInfo.inputPath, "*.jpg");
        for (int i = 0; i < files.Length; i++)
        {
            File.Delete(files[i]);
        }
    }

    private void OnAudioImportComplete(object sender, EventArgs e)
    {
        UnityEngine.Debug.Log("Audio import Complete");
    }

    private void ParseFFMPEGStream(StreamReader reader)
    {
        string line;
        while ((line = reader.ReadLine()) != null)
        {
            try
            {
                this.videoImportInfo = videoImportInfo.ParseLine(line);
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.Log("Parsing FFMPEG feedback failed, invalid line" + e.ToString());
            }
        }
    }

    private void StartProcessInNewThread(Process process)
    {
        var thread = new Thread(delegate ()
        {
            try
            {
                process.Start();
                //if there is an incoming stream, then parse it
                if (process.StartInfo.RedirectStandardError)
                {
                    StreamReader sr = process.StandardError;
                    ParseFFMPEGStream(sr);
                }
                process.WaitForExit();
                process.Close();
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.LogError(e.ToString());
            }
        });
        thread.Start();
    }
}
