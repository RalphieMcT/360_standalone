﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;
using System.Linq;

public class CameraSpawner : MonoBehaviour
{
    public GameObject cameraPrefab;
    private List<GameObject> allCameras = new List<GameObject>();
    public enum DisplayType
    {
        SingleMonitor,
        DoubleMonitor,
        MultiProjector8,
        MultiProjector12
    }
    [SerializeField]
    DisplayType type;

    void Start()
    {
        CameraRigConfig cameraRigConfig = new CameraRigConfig(type);
        SpawnCameras(cameraRigConfig);
        ActivateDisplays(cameraRigConfig);
        SetCanvasProperties(cameraRigConfig);
    }

    private void ActivateDisplays(CameraRigConfig cameraRigConfig)
    {
        for (int i = 0; i < Display.displays.Length; i++)
        {
            Display.displays[i].Activate();
        }
        
        for (int i = 0; i < allCameras.Count; i++)
        {
            allCameras[i].GetComponent<Camera>().targetDisplay = cameraRigConfig.usedProjectors[i];
        }
    }


    private void SetCanvasProperties(CameraRigConfig cameraRigConfig)
    {
        Canvas UICanvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        UICanvas.renderMode = RenderMode.ScreenSpaceOverlay;
        UICanvas.targetDisplay = cameraRigConfig.UILocation;
    }

    private void SpawnCameras(CameraRigConfig cameraRigConfig)
    {
        for (int i = 0; i < cameraRigConfig.cameraConfigPaths.Length; i++)
        {
            GameObject camera = Instantiate(cameraPrefab);
            camera.transform.parent = transform;
            camera.name = new DirectoryInfo(cameraRigConfig.cameraConfigPaths[i]).Name; //name of current camera directory
            string MainConfig = cameraRigConfig.cameraFilesPath + @"\" + camera.name + @"\mainConfig.xml";
            camera.GetComponent<CameraConfig>().LoadCameraConfiguration(MainConfig);
            camera.transform.localPosition = camera.GetComponent<CameraConfig>().Position;
            camera.transform.eulerAngles = -RadiansToDegrees(camera.GetComponent<CameraConfig>().Rotation);
            allCameras.Add(camera);
        }

        //remove default camera
        if (transform.FindChild("Camera").gameObject != null)
            DestroyImmediate(transform.FindChild("Camera").gameObject);

        //turn on warping blending and stencil shaders
        Camera[] cams = FindObjectsOfType<Camera>();
        foreach (Camera c in cams)
        {
            c.gameObject.GetComponent<Warping>().enabled = true;
            c.gameObject.GetComponent<Blending>().enabled = true;
            c.gameObject.GetComponent<Stencil>().enabled = true;
        }
    }

    private Vector3 RadiansToDegrees(Vector3 radVector)
    {
        Vector3 degreeVector = new Vector3();
        degreeVector.x = Mathf.Rad2Deg * radVector.x;
        degreeVector.y = Mathf.Rad2Deg * radVector.y;
        degreeVector.z = Mathf.Rad2Deg * radVector.z;
        return degreeVector;
    }
}
