﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
   
[ExecuteInEditMode]
public class Warping : ImageEffectBase
{
    
    protected override void Start()
    {
        base.Start();
        string fullTexturePath = RelativeToFullPath(gameObject.name);
        Texture2D warpTexture = TextureLoader.LoadRawTexture(fullTexturePath, TextureFormat.RGBAFloat);
        material.SetTexture("_WarpTex", warpTexture);
    }

    private string RelativeToFullPath(string cameraName)
    {
        string startOfPath = Path.Combine(Application.dataPath, @"StreamingAssets/CameraFiles");
        startOfPath = Path.Combine(startOfPath, cameraName);
        string endOfPath = gameObject.GetComponent<CameraConfig>().DistortionTexture;
        endOfPath = endOfPath.Substring(2, endOfPath.Length - 2);
        string fullTexturePath = Path.Combine(startOfPath, endOfPath);
        return fullTexturePath;
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, material);
    }
}
