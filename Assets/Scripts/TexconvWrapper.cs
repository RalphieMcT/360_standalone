﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TexconvWrapper
{
    List<string> args = new List<string>();
    private string texconvPath;
    private string inputPath;

    public TexconvWrapper SetOutputPath(string path)
    {
        Debug.Log("SetOutPutPath(): " + path);
        args.Add(" -o " + "\"" + path + "\"");
        return this;
    }

    /// <summary>
    /// The folder containing the JPEG files to convert. Accepts wildcards.
    /// </summary>
    /// <param name="path">The folder containing the JPEG files to convert</param>
    /// <param name="pattern">a pattern that specifies the files to convert. e.g. "*.jpg"</param>
    /// <returns></returns>
    public TexconvWrapper SetInputPath(string path, string pattern)
    {
        inputPath = (" " + "\"" + path + "/" + pattern + "\"").Replace("/", @"\");
        return this;
    }

    public TexconvWrapper SetFormat(string format)
    {
        args.Add(" -f " + format);
        return this;
    }

    public TexconvWrapper VerticalFlip()
    {
        args.Add(" -vflip ");
        return this;
    }

    public TexconvWrapper HorizontalFlip()
    {
        args.Add(" -hflip ");
        return this;
    }

    public TexconvWrapper SetNumberOfMipMaps(int numMipMaps)
    {
        args.Add(" -m " + numMipMaps.ToString());
        return this;
    }

    public TexconvWrapper EnableTiming()
    {
        args.Add(" -timing ");
        return this;
    }

    public TexconvWrapper CPUOnly()
    {
        args.Add(" -nogpu ");
        return this;
    }

    public TexconvWrapper ForceOverwrite()
    {
        args.Add(" -y ");
        return this;
    }

    public TexconvWrapper SetTexconvPath(string path)
    {
        texconvPath = "\"" + path + "\"";
        return this;
    }

    public override string ToString()
    {
        string texconvArguments = texconvPath;
        foreach (string arg in args)
        {
            texconvArguments += arg;
        }
        texconvArguments += inputPath;
        return texconvArguments;
    }
}
