﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class PanelExpander : MonoBehaviour {
    Animator button;
    public Animator optionsPanel;
    public PanelButtons pb;

    void Start() {
        button = GetComponent<Animator>();
    }

    public void ToggleVisible() {
        bool currentState = button.GetBool("isExpanded");
        if(!currentState) {
            pb.updateFileList();
        }
        button.SetBool("isExpanded", !currentState);
        optionsPanel.SetBool("isExpanded", !currentState);
    }
}
