﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

/// <summary>
/// PanelButtons responsibility is to instantiate the UI buttons for choosing
/// videos, and a button to import videos.
/// </summary>

public class PanelButtons : MonoBehaviour {
    public GameObject btnPrefab;
    public GameObject btnImportPrefab;

    private void Awake()
    {
        Directory.CreateDirectory(Application.streamingAssetsPath + "/DDSVideos");
        Directory.CreateDirectory(Application.streamingAssetsPath + "/OggVideos");
    }

    //TODO: ignore currently importing video
    public void updateFileList() {
        string[] DDSVideos = Directory.GetDirectories(Application.streamingAssetsPath + "/DDSVideos"); //the folders in /resources are .jpeg videos
        string[] oggVideos = Directory.GetFiles(Application.streamingAssetsPath + "/OggVideos", "*.ogg");
        string[] videos = new string[DDSVideos.Length + oggVideos.Length];
        DDSVideos.CopyTo(videos, 0);
        oggVideos.CopyTo(videos, DDSVideos.Length); //join arrays
        
        for(int i = 0; i < videos.Length; i++) {
            videos[i] = videos[i].Split('\\')[1]; //remove everything before backslash
        }
        CreateButtons(videos);
    }

    void CreateButtons(string[] videos) {
        //destroy buttons
        for(int i = 0; i < transform.childCount; i++){
            Destroy(transform.GetChild(i).gameObject);
        }

        //create a button for each video
        foreach(string s in videos) {
            GameObject button = Instantiate(btnPrefab);
            button.name = s;
            button.transform.SetParent(transform);
            button.transform.GetComponentInChildren<Text>().text = s;
        }

        //create import button
        GameObject importButton = Instantiate(btnImportPrefab);
        importButton.transform.SetParent(transform);
        importButton.name = "Import";
        importButton.transform.GetComponentInChildren<Text>().text = "Import";
    }
}
