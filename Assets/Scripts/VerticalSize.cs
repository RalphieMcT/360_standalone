﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VerticalSize : MonoBehaviour {
    public float childHeight = 50f;
    RectTransform panelTransform;

    void Start () {
        panelTransform = GetComponent<RectTransform>();
	}
	

	void Update () {
        Vector2 deltaPanelSize = panelTransform.sizeDelta;
        deltaPanelSize.y = transform.childCount * childHeight;
        panelTransform.sizeDelta = deltaPanelSize;

        Vector2 panelPos = panelTransform.position;
        panelPos.y = childHeight/2*transform.childCount;
        panelTransform.position = panelPos;

    }
}
