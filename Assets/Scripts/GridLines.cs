﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridLines : MonoBehaviour
{
    public GameObject torus;
    [Range(0, 10)]
    public int horizontalCircles;
    [Range(0, 5)]
    public int verticalCircles;
    private Transform gridParent;
    private float videoSphereSize;
    
    void Start ()
    {
        Transform videoSphere = GameObject.Find("VideoWall").transform;
        gridParent = new GameObject("GridParent").transform;
        videoSphereSize = videoSphere.localScale.y;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.G))
        {
            if(gridParent.childCount == 0)
            {
                SpawnGridLines();
            }
            else
            {
                RemoveGridLines();
            }
        }
    }

    private void SpawnGridLines()
    {
        for (int i = 0; i < horizontalCircles; i++)
        {
            float y = videoSphereSize * i / (2 * horizontalCircles);
            Quaternion horizontalRotation = Quaternion.Euler(90, 0, 0);
            if (i == 0)
            {
                SpawnTorus(torus, new Vector3(0, y, 0), horizontalRotation, 1);
            }
            else
            {
                float sphereHeightScaling = Mathf.Sqrt(1 - Mathf.Pow((float) i/horizontalCircles, 2));
                SpawnTorus(torus, new Vector3(0, y, 0), horizontalRotation, sphereHeightScaling);
                SpawnTorus(torus, -new Vector3(0, y, 0), horizontalRotation, sphereHeightScaling);
            }
        }

        int totalNumCircles = (int) Mathf.Pow(2f, verticalCircles) / 2;
        for (int i = 0; i < totalNumCircles; i++)
        {
            SpawnTorus(torus, Vector3.zero, Quaternion.Euler(0, i*90f / totalNumCircles, 0), 1);
            SpawnTorus(torus, Vector3.zero, Quaternion.Euler(0, i*90f / totalNumCircles + 90f, 0), 1);
        }
    }

    private void SpawnTorus(GameObject torusObject, Vector3 torusCenter, Quaternion rotation, float XYScaling)
    {
        GameObject newTorus = Instantiate(torusObject, torusCenter, rotation, gridParent);
        newTorus.transform.localScale = Vector3.Scale(new Vector3(1, 1, 1), new Vector3(XYScaling, XYScaling, 2));
    }

    private void RemoveGridLines()
    {
        var children = new List<GameObject>();
        foreach (Transform child in gridParent) children.Add(child.gameObject);
        for (int i = 0; i < children.Count; i++)
        {
            Destroy(children[i]);
        }
    }
}
