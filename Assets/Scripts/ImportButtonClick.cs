﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SFB;

public class ImportButtonClick : MonoBehaviour
{

    public void OnClick()
    {
        string[] filePath = StandaloneFileBrowser.OpenFilePanel("Open File", "", "", true);
        if (filePath.Length > 0)
            ImportFile(filePath[0]);
    }

    protected void ImportFile(string path)
    {
        GameObject progressPanel = GameObject.Find("Canvas/ProgressPanel");
        GameObject guiManager = GameObject.Find("Canvas/VideoControls");
        try
        {
            GameObject videoWall = GameObject.Find("VideoWall");
            VideoScript vs = videoWall.GetComponent<VideoScript>();
            vs.StartVideo(path);
            guiManager.GetComponent<GUIManager>().CloseButtonListAndOptionPanel();
        }
        catch (System.Exception e)
        {
            Debug.LogException(e);
            Debug.Log("Input Path: " + path);
            Debug.LogError("Cant import that file: " + e.ToString());
        }
    }
}
