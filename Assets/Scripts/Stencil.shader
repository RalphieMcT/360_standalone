﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Hidden/Stencil"
{
	Properties
	{
		_MainTex("Texture", 2D) = "black" {}
		_StencilTex("Stencil", 2D) = "black" {}
	}
		SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
	{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"

		struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 vertex : SV_POSITION;
	};

	v2f vert(appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = v.uv;
		return o;
	}

	sampler2D _MainTex;
	sampler2D _StencilTex;
	float4 _MainTex_TexelSize;

	float4 frag(v2f i) : SV_Target
	{

	//Flip texture if DirectX
	#if UNITY_UV_STARTS_AT_TOP
			i.uv.y = 1 - i.uv.y;
	#endif

	//compensate for 90degree texture rotation
	float tmpX = i.uv.x;
	float tmpY = 1 - i.uv.y;
	float2 tmpUV = float2(tmpX, tmpY);
	i.uv.x = 1 - i.uv.y;
	i.uv.y = tmpX;

	//i.uv.y = 1 - i.uv.y;
	float4 StencilTexture = tex2D(_StencilTex, i.uv);
	//un-compensate for texture rotation
	StencilTexture.y = 1 - StencilTexture.y;

	//flip coordinates to correct orientation
	float4 MainTexture;
	//if warp texture is black, render black
	if (StencilTexture.a == 0) {
		MainTexture = float4(0.0,0.0,0.0,0.0);
	}
	else {
		MainTexture = tex2D(_MainTex, tmpUV);
	}
	return MainTexture;
	}
		ENDCG
	}
	}
		Fallback off
}
